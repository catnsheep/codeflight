// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "GameFramework/Pawn.h"
#include "PlayerControlCenter.generated.h"

UCLASS()
class FLIGHTPROTOTYPEONE_API APlayerControlCenter : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerControlCenter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	virtual void GetMoveAreaBoundary(float& OutHorizontalMoveMin, float& OutHorizontalMoveMax, float& OutVerticalMoveMax, float& OutVerticalMoveMin);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* MainCameraSpringArm;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* MainCamera;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UChildActorComponent* PlayerCharacter;

	/************************************************************************/
	/* Character Movement Parameter                                         */
	/************************************************************************/
	/** Normal movespeed of character */
	UPROPERTY(EditAnywhere)
	float CharacterMoveSpeed;
	UPROPERTY(EditAnywhere)
	float RollLimit;
	UPROPERTY(EditAnywhere)
	float RollSpeed;
	//transient value for roll action
	float CurRollTarget;
	//if player is in firing status
	bool bIsFiring;

	/************************************************************************/
	/* Move Area and Camera Constraint                                      */
	/************************************************************************/
	/** Used for initialize field of view, character and camera movement constraint */
	UPROPERTY(EditAnywhere)
	float WidthConstraint;
	/** Height Constraint is calculated based on camera aspect ratio and WidthConstraint */
	float HeightConstraint;

	/** Current movement limitation */
	FVector CharacterMoveAreaMax;
	FVector CharacterMoveAreaMin;

	/** Overall max/min move constraint */
	float HorizontalMoveMin;
	float HorizontalMoveMax;
	float VerticalMoveMax;
	float VerticalMoveMin;

	/************************************************************************/
	/* Cache values                                                         */
	/************************************************************************/
	// 1/2 of the vertical field of view value
	float VerticalFOVHalf;
	// 1/2 of the horizontal field of view value in radians
	float HFOVRadiansHalf;
	// a constant relative height of the camera
	float CameraRelativeHeight;
	// min camera pitch angle
	float CameraVerticalMin;
	// max camera pitch angle
	float CameraVerticalMax;

	/** Adjust camera position and rotation based on player character position slightly */
	void AdjustCamera();

	//cache move input axises
	FVector MovementInput;
	void VerticalMove(float AxisValue);
	void HorizontalMove(float AxisValue);
	void HandleCharacterMovement(float DeltaTime);
	void HoldCharacterInCamera(float DeltaTime);

	void StartFiring();
	void StopFiring();

#if WITH_EDITOR
	void DrawMoveArea();
#endif

};
