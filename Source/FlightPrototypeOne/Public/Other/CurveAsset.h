// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "Engine/DataAsset.h"
#include "CurveAsset.generated.h"

USTRUCT(BlueprintType)
struct FCurvePoint
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FVector Location;
	UPROPERTY(EditAnywhere)
	FVector ArriveTangent;
	UPROPERTY(EditAnywhere)
	FVector LeaveTangent;
	UPROPERTY(EditAnywhere)
	TEnumAsByte<enum EInterpCurveMode> InterpMode;
};

UCLASS(BlueprintType)
class FLIGHTPROTOTYPEONE_API UCurveAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	FVector RangeContext;

	//The location info within this array are relative to the central point of the boundary, location point should be ranged from -1 to 1
	UPROPERTY(EditAnywhere)
	TArray<FCurvePoint> CurvePoints;

};
