// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "Factories/Factory.h"
#include "CurveAssetFactory.generated.h"

/**
 * 
 */
UCLASS()
class FLIGHTPROTOTYPEONE_API UCurveAssetFactory : public UFactory
{
	GENERATED_BODY()
public:
	UCurveAssetFactory();
	virtual UObject* FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override;
	
	
};
