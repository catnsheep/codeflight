// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "Engine/DataAsset.h"
#include "Item/BasicPoolObject.h"
#include "CurveAsset.h"

#include "LevelFlowAsset.generated.h"

USTRUCT(BlueprintType)
struct FWaveEnemy
{
	GENERATED_USTRUCT_BODY()
	//when this wave should be generated
	UPROPERTY(EditAnywhere)
	float TimeInSeconds;
	//type of the enemy
	UPROPERTY(EditAnywhere)
	TSubclassOf<ABasicPoolObject> EnemyType;
	//the curve file to be used
	UPROPERTY(EditAnywhere)
	UCurveAsset* CurveAsset;
	//Direction of following the curve
	UPROPERTY(EditAnywhere)
	bool ReverseTravel = false;
	//a curve asset for balancing the speed based on the original value over the spline
	UPROPERTY(EditAnywhere)
	UCurveFloat* VelocityBalancingCurve;
	//number generated
	UPROPERTY(EditAnywhere)
	int32 NumberToGenerate;
	//delay between two generation
	UPROPERTY(EditAnywhere)
	float GenerateDelay;
};

UCLASS(BlueprintType)
class FLIGHTPROTOTYPEONE_API ULevelFlowAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	//the enemy wave data
	UPROPERTY(EditAnywhere)
	TArray<FWaveEnemy> Waves;
	
};
