// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once
#include "CurveEditor.h"
#include "Editor/DetailCustomizations/Private/DetailCustomizationsPrivatePCH.h"

/**
 * 
 */
class FLIGHTPROTOTYPEONE_API FCurveEditorDetails : public IDetailCustomization
{
public:
	/** Makes a new instance of this detail layout class for a specific detail view requesting it */
	static TSharedRef<IDetailCustomization> MakeInstance();

	/** IDetailCustomization interface */
	virtual void CustomizeDetails(IDetailLayoutBuilder& DetailBuilder) override;

private:
	/** Internal function for generating curve DataAsset */
	FReply OnGenerateDataAsset();
	/** The curve editor instance we are operating on*/
	TWeakObjectPtr<ACurveEditor> CurveEditor;
	/** Name of asset to be created */
	FString AssetName;
	
	void OnAssetNameChanged(const FText& Text);
	void OnAssetNameCommitted(const FText& Text, ETextCommit::Type CommitType);
};
