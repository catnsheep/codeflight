// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "Factories/Factory.h"
#include "LevelFlowAssetFactory.generated.h"

/**
 * 
 */
UCLASS()
class FLIGHTPROTOTYPEONE_API ULevelFlowAssetFactory : public UFactory
{
	GENERATED_BODY()
public:
	ULevelFlowAssetFactory();
	virtual UObject* FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override;
};
