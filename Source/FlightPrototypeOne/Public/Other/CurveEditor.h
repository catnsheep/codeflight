// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SplineComponent.h"

#include "CurveEditor.generated.h"

UCLASS()
class FLIGHTPROTOTYPEONE_API ACurveEditor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Set the boundary limit
	UPROPERTY(EditAnywhere)
	FVector BoundarySize;
	// Show a debug line on half of the box in y axis
	UPROPERTY(EditAnywhere)
	bool ShowHalfYAxis = false;
	// Spline Component to modify and generate data
	UPROPERTY(EditAnywhere)
	USplineComponent* CustomSpline;

	// Sets default values for this actor's properties
	ACurveEditor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	
	void SaveCurveDataAsset(FString& FileName);

protected:
	// Call to refresh and draw a reference debug box
	void UpdateBoundary();
	// Helper function to constraint the spline with in the boundary range
	void ConstraintSpline();


	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void BeginDestroy() override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	virtual void PostEditMove(bool bFinished) override;
#endif

};
