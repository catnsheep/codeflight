// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "GameFramework/GameState.h"
#include "FlightGameState.generated.h"

UENUM()
enum class EGameStatus : uint8
{
	BeforeStart,
	GameInProgress,
	BeforeEnd
};

UCLASS()
class FLIGHTPROTOTYPEONE_API AFlightGameState : public AGameState
{
	GENERATED_BODY()
public:
	AFlightGameState();

	//current progress of player in game
	EGameStatus CurrentState;
	
};
