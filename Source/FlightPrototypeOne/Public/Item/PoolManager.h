// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "GameFramework/Actor.h"
#include "BasicPoolObject.h"

#include "PoolManager.generated.h"

USTRUCT()
struct FPoolObjectEntry
{
	GENERATED_USTRUCT_BODY()

	//the object to be managed by object bool
	UPROPERTY(EditAnywhere)
	TSubclassOf<ABasicPoolObject> PoolObject;

	//initial amount of the object in pool
	UPROPERTY(EditAnywhere)
	int32 InitialAmount;
};

UCLASS()
class FLIGHTPROTOTYPEONE_API APoolManager : public AActor
{
	GENERATED_BODY()

protected:
	TMap<FString, TSharedPtr<TArray<ABasicPoolObject*>>> PoolTable;
	static APoolManager* instance;

public:	
	// Sets default values for this actor's properties
	APoolManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	//virtual void Tick( float DeltaSeconds ) override;
	
	// Register all items need to be managed by pool here
	UPROPERTY(EditAnywhere)
	TArray<FPoolObjectEntry> PoolObjectEntrys;

	//try generate a item from objects pool, if not item available, will spawn a new one
	UFUNCTION()
	ABasicPoolObject* TryGenerateFromPool(TSubclassOf<ABasicPoolObject> Target, FVector IniLocation, FRotator IniRotation, float IniLifeTime = -1);
	//generate at the place of the pool manager (probably somewhere out of the world, to avoid invalid collision)
	UFUNCTION()
	ABasicPoolObject* TryGenerateFromPoolNoPos(TSubclassOf<ABasicPoolObject> Target, float IniLifeTime = -1);

	static APoolManager* GetInstance();

};
