// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "Public/Item/BasicPoolObject.h"
#include "BasicBullet.generated.h"

/**
 * Basic class for a forward moving bullet
 */
UCLASS()
class FLIGHTPROTOTYPEONE_API ABasicBullet : public ABasicPoolObject
{
	GENERATED_BODY()
	
public:
	ABasicBullet();

	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
	
	// If the bullet is shoot by player
	bool bFromPlayer;

	UPROPERTY(EditAnywhere)
	float SingleDamage = 20;

protected:
	/** Forward moving speed */
	UPROPERTY(EditAnywhere, Category = Movement)
	float MoveSpeed;
	
};
