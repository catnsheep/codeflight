// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "GameFramework/Actor.h"
#include "BasicPoolObject.h"

#include "BasicWeapon.generated.h"

USTRUCT()
struct FWeaponSocket
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	TSubclassOf<ABasicPoolObject> BulletType;

	//relative spawn location for bullets
	UPROPERTY(EditAnywhere)
	FVector SpawnLocation;

	//relative spawn rotation for bullets
	UPROPERTY(EditAnywhere)
	FRotator SpawnRotation;

};

UCLASS()
class FLIGHTPROTOTYPEONE_API ABasicWeapon : public AActor
{
	GENERATED_BODY()
	
protected:
	// delay between two continues fire
	float FireDelay;

public:	
	// Sets default values for this actor's properties
	ABasicWeapon();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION()
	virtual void Fire(float DeltaSeconds);
	
	UPROPERTY(EditAnywhere)
	TArray<FWeaponSocket> SocketDefinition;

	UPROPERTY(EditAnywhere)
	float DefaultFireDelay;

	AActor* OwningPlayer;
	//if the weapon is owned by a player
	bool bOwnedByPlayer;
};
