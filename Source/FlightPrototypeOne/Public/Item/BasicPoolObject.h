// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "GameFramework/Actor.h"
#include "BasicPoolObject.generated.h"

UCLASS()
class FLIGHTPROTOTYPEONE_API ABasicPoolObject : public AActor
{
	GENERATED_BODY()

protected:
	//if the projectile is in activated
	bool bActivated;
	//life time remaining when projectile is activated
	float lifeTime;
	//if the object is dynamic generated or managed by objects pool
	bool bDynamicGenerated;

public:
	// Sets default values for this actor's properties
	ABasicPoolObject();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	/** Maximum lifetime when projectile is activated, set to <0 initial value gives infinite life time */
	UPROPERTY(EditAnywhere, Category = Life)
	float MaxLifeTime;

	/** Use this function to activate an object either from object pool or dynamic generated, return false if actor has already been activated */
	UFUNCTION()
	bool Activate(FVector IniLocation, FRotator IniRotation, float IniLifeTime, bool bDynamicGen);

	/** Use this function to de-activate an object when life time is over */
	UFUNCTION()
	void Deactivate();

	//location for object in objects pool
	UPROPERTY()
	FVector InValidLocation;

	//parent when the object is in invalid state
	UPROPERTY()
	AActor* InValidParent;

	UFUNCTION()
	bool IsActivated();

};
