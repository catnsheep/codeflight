// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "GameFramework/GameMode.h"
#include "FlightGameMode.generated.h"

/**
 * 
 */
UCLASS()
class FLIGHTPROTOTYPEONE_API AFlightGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	AFlightGameMode(const FObjectInitializer& ObjectInitializer);

	/** Relative speed of environment moving backward */
	UPROPERTY(EditAnywhere)
	float EnvironmentMoveSpeed;

	UPROPERTY(EditAnywhere, Category = "LevelConfig")
	UClass* PlayerControlCenter;
	UPROPERTY(EditAnywhere, Category = "LevelConfig")
	UClass* EnemyManager;

	UFUNCTION(BlueprintCallable, Category = "Game")
	virtual void StartPlay() override;

};
