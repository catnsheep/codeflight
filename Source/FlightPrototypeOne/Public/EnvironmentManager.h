// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "GameFramework/Actor.h"
#include "EnvironmentActor.h"

#include "EnvironmentManager.generated.h"

UCLASS()
class FLIGHTPROTOTYPEONE_API AEnvironmentManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnvironmentManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere)
	UClass* GroundActor;

	TArray<AEnvironmentActor*> EnvironmentActors;

};
