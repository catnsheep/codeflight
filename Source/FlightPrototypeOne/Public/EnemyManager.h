// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "Player/PlayerControlCenter.h"
#include "GameFramework/Actor.h"
#include "Other/CurveAsset.h"
#include "Runtime/Engine/Classes/Components/SplineComponent.h"
#include "Item/BasicPoolObject.h"
#include "Character/BasicEnemy.h"
#include "Other/LevelFlowAsset.h"
#include "FlightGameState.h"

#include "EnemyManager.generated.h"

UCLASS()
class FLIGHTPROTOTYPEONE_API AEnemyManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemyManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Initialize the enemy manager based on Player move area, it is called after begin play
	virtual void Init(APlayerControlCenter* PCC);
	
	UPROPERTY(EditAnywhere)
	float ZSpaceExtension = 50;

	//the enemy data for  current level
	UPROPERTY(EditAnywhere)
	ULevelFlowAsset* LevelDefinition;

protected:
	UPROPERTY()
	APlayerControlCenter* PlayerControlCenter;
	//Values we need to construct a enemy move area which is the "double" height of player move area
	FVector CenterReferencePoint;
	float VerticalHalf;
	float HorizontalMin;
	float HorizontalDiff;

	UPROPERTY()
	TMap<FString, USplineComponent*> SplineTable;

	//Helper function for converting vector point from curve data to actual world space
	FVector SplinePointToAreaPoint(FVector& Pos);
	//the time that the game has run without pause
	float InProgressGameTime;
	//the next wave of enemy to be generated
	int nextWave;

	//Helper function for generating a wave of enemies
	void GenerateSingleEnemy(FWaveEnemy* EnemyWave);
	void GenerateWaveEnemies(FWaveEnemy& EnemyWave);

#if WITH_EDITOR
	void DrawEnemyMoveArea();
#endif

};
