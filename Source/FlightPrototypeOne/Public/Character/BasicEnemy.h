// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "Public/Item/BasicPoolObject.h"
#include "Public/Item/BasicWeapon.h"
#include "Other/CurveAsset.h"
#include "Runtime/Engine/Classes/Components/SplineComponent.h"

#include "BasicEnemy.generated.h"

UCLASS()
class FLIGHTPROTOTYPEONE_API ABasicEnemy : public ABasicPoolObject
{
	GENERATED_BODY()
	
public:
	ABasicEnemy();

	/** The weapon this character possess */
	UPROPERTY(EditAnywhere, Category = Weapon)
	TSubclassOf<ABasicWeapon> BasicWeapon;
	
	/** If this character will use weapon */
	UPROPERTY(EditAnywhere, Category = Weapon)
	bool UseWeapon;

	virtual void BeginPlay() override;

	virtual void Init(USplineComponent* SplineToUse, bool bReverseTravel, UCurveFloat* VelocityCurveToUse);

	virtual void Tick(float DeltaSeconds) override;

	virtual void Fire(float DeltaSeconds);
	
	virtual void MountWeapon(TSubclassOf<ABasicWeapon> Weapon);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* CharacterMesh;

	UPROPERTY(EditAnywhere, Category = Movement)
	float MoveSpeed = 10;

	UPROPERTY(EditAnywhere, Category = Character)
	float TotalHealth = 100;

protected:
	ABasicWeapon* CurrentWeapon;
	//reference to the actual movement spline
	UPROPERTY()
	USplineComponent* MoveSpline;
	//A curve for controlling velocity over the spline
	UPROPERTY()
	UCurveFloat* VelocityCurve;

	bool ReverseTravel;
	float CurrentDistance;
	float TotalPathLength;
	float CurrentHealth;

	void UpdateLocationRotationOnSpline(float DeltaSeconds); 

	UFUNCTION()
	void OnBodyOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

};
