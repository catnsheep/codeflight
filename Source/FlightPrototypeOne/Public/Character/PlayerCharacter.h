// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "GameFramework/Actor.h"
#include "Public/Item/BasicWeapon.h"

#include "PlayerCharacter.generated.h"

UCLASS()
class FLIGHTPROTOTYPEONE_API APlayerCharacter : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void Fire(float DeltaSeconds);

	virtual void MountWeapon(TSubclassOf<ABasicWeapon> Weapon);

	//the basic weapon the character equiped with
	UPROPERTY(EditAnywhere)
	TSubclassOf<ABasicWeapon> BasicWeapon;
	//the maximum health point of the character
	UPROPERTY(EditAnywhere)
	float MaxHealthPoint = 1;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* CharacterMesh;
	//A colliding sphere for hit detection
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCapsuleComponent* HitSphere;

	//the current weapon the character is holding
	ABasicWeapon* CurrentWeapon;
	//the current HP the character have
	float CurrentHealth;

	UFUNCTION()
	void OnHitDetected(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void HitWithDamage(float damage);

};
