// Copyright 2015, Maoyang Li. All Rights Reserved

#pragma once

#include "GameFramework/Actor.h"
#include "EnvironmentActor.generated.h"

USTRUCT(BlueprintType)
struct FHorizontalSpawnLine
{
	GENERATED_USTRUCT_BODY();

	/** Minimum location.x for this line */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float MinX;
	/** Maximum location.x for this line */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float MaxX;
};

UCLASS()
class FLIGHTPROTOTYPEONE_API AEnvironmentActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnvironmentActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	/************************************************************************/
	/* BP Initialized Variables                                             */
	/************************************************************************/
	/** Minimum possible scale value for actor */
	UPROPERTY(EditAnywhere)
	FVector MinScale;
	/** Maximum possible scale value for actor */
	UPROPERTY(EditAnywhere)
	FVector MaxScale;	
	/** Minimum possible rotation value for actor */
	UPROPERTY(EditAnywhere)
	FRotator MinRotation;
	/** Maximum possible rotation value for actor */
	UPROPERTY(EditAnywhere)
	FRotator MaxRotation;
	/** Height for each of the actor, z of location vector */
	UPROPERTY(EditAnywhere)
	float ActorLocationHeight;
	/** Minimum horizontal location x for initialize */
	UPROPERTY(EditAnywhere)
	float MinHorizontal;
	/** Maximum  horizontal location x for initialize */
	UPROPERTY(EditAnywhere)
	float MaxHorizontal;
	/** The maximum location y ahead of actor for environment actor initialize */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float ForwardLimit;
	/** Reset limit, when environment actor location y is smaller then this, report and got reset */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float ResetLimit;
	/** Step between two spawns */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float SpawnStep;
	/** Possible "Line" of spawn locations*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FHorizontalSpawnLine> Lines;
	/** If a slope height constraint will be applied */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bApplyHeightConstraint;
	
	void ResetActor(int32 LineIndex, float VerticalLocation, float Speed);

protected:
	/************************************************************************/
	/* Generator Initialized Variables                                      */
	/************************************************************************/
	//move speed (set by generator)
	float MoveSpeed;
	//initial horizontal position
	int32 IniLine;

	//calculate slope height limit based on Line index 0
	float CalculateSlopeHeightLimit(float x);

};
