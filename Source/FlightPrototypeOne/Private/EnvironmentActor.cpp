// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "EnvironmentActor.h"

// Sets default values
AEnvironmentActor::AEnvironmentActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));


}

// Called when the game starts or when spawned
void AEnvironmentActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnvironmentActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	FVector ActorLocation = GetActorLocation();
	ActorLocation.Y -= DeltaTime * MoveSpeed;
	SetActorLocation(ActorLocation); 
	if (ActorLocation.Y < ResetLimit)
	{
		ResetActor(IniLine, ForwardLimit, MoveSpeed);
	}

}

void AEnvironmentActor::ResetActor(int32 LineIndex, float VerticalLocation, float Speed)
{
	MoveSpeed = Speed;
	//initial location
	float x = 0;
	if (Lines.IsValidIndex(LineIndex))
	{
		x = FMath::FRandRange(Lines[LineIndex].MinX, Lines[LineIndex].MaxX);
	}
	SetActorLocation(FVector(x, VerticalLocation, ActorLocationHeight));
	
	//initial scale
	FVector IScale;
	IScale.X = MinScale.X < MaxScale.X ? FMath::FRandRange(MinScale.X, MaxScale.X) : MaxScale.X;
	IScale.Y = MinScale.Y < MaxScale.Y ? FMath::FRandRange(MinScale.Y, MaxScale.Y) : MaxScale.Y;
	float CurMaxZ = MaxScale.Z;
	if (bApplyHeightConstraint)
	{
		float CurLimitZ = CalculateSlopeHeightLimit(x);
		if (CurLimitZ < CurMaxZ)
		{
			CurMaxZ = CurLimitZ;
		}
	}
	IScale.Z = MinScale.Z < CurMaxZ ? FMath::FRandRange(MinScale.Z, CurMaxZ) : CurMaxZ;
	SetActorScale3D(IScale);

	//initial rotation
	FRotator IRotator;
	IRotator.Yaw = MinRotation.Yaw < MaxRotation.Yaw ? FMath::FRandRange(MinRotation.Yaw, MaxRotation.Yaw) : MaxRotation.Yaw;
	IRotator.Pitch = MinRotation.Pitch < MaxRotation.Pitch ? FMath::FRandRange(MinRotation.Pitch, MaxRotation.Pitch) : MaxRotation.Pitch;
	IRotator.Roll = MinRotation.Roll < MaxRotation.Roll ? FMath::FRandRange(MinRotation.Roll, MaxRotation.Roll) : MaxRotation.Roll;
	SetActorRotation(IRotator);

	IniLine = LineIndex;
}

float AEnvironmentActor::CalculateSlopeHeightLimit(float x)
{
	if (Lines.Num()==0)
	{
		return 0;
	}
	float AbsX = FMath::Abs(x);
	float AbsX0 = FMath::Abs(Lines[0].MinX);
	float AbsX1 = FMath::Abs(Lines[0].MaxX);
	if (AbsX0 > AbsX1)
	{
		float t = AbsX0;
		AbsX0 = AbsX1;
		AbsX1 = t;
	}

	float k = (MaxScale.Z - MinScale.Z) / AbsX1 - AbsX0;
	float d = MaxScale.Z - k * AbsX1;
	return k * AbsX + d;
}
