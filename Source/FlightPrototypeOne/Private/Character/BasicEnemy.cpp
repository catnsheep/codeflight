// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "Public/Character/BasicEnemy.h"
#include "Public/Item/BasicBullet.h"

ABasicEnemy::ABasicEnemy()
{
	UseWeapon = false;

	CharacterMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CharacterMesh"));
	CharacterMesh->AttachTo(RootComponent);
	CharacterMesh->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
	CharacterMesh->SetCollisionProfileName("CharacterHitDetect");
	CharacterMesh->OnComponentBeginOverlap.AddDynamic(this, &ABasicEnemy::OnBodyOverlapBegin);

	MaxLifeTime = 36000.f;
}

void ABasicEnemy::BeginPlay()
{
	Super::BeginPlay();
	if (UseWeapon)
	{
		MountWeapon(BasicWeapon);
	}
	CurrentHealth = TotalHealth;
}

void ABasicEnemy::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (bActivated)
	{
		Fire(DeltaSeconds);

		if (CurrentDistance <= TotalPathLength)
		{
			UpdateLocationRotationOnSpline(DeltaSeconds);
		}
		else
		{
			Deactivate();
		}
	}
}

void ABasicEnemy::Fire(float DeltaSeconds)
{
	if ( UseWeapon && CurrentWeapon)
	{
		CurrentWeapon->Fire(DeltaSeconds);
	}
}

void ABasicEnemy::MountWeapon(TSubclassOf<ABasicWeapon> Weapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
	}
	UWorld* const World = GetWorld();
	CurrentWeapon = World->SpawnActor<ABasicWeapon>(*Weapon, GetActorLocation(), GetActorRotation());
	CurrentWeapon->OwningPlayer = this;
	CurrentWeapon->bOwnedByPlayer = false;
}

void ABasicEnemy::Init(USplineComponent* SplineToUse, bool bReverseTravel, UCurveFloat* VelocityCurveToUse)
{
	VelocityCurve = VelocityCurveToUse;
	ReverseTravel = bReverseTravel;
	MoveSpline = SplineToUse;
	TotalPathLength = MoveSpline->GetSplineLength();
	CurrentDistance = 0;
	UpdateLocationRotationOnSpline(0);
}

void ABasicEnemy::UpdateLocationRotationOnSpline(float DeltaSeconds)
{
	if (ReverseTravel)
	{
		CurrentDistance += MoveSpeed * DeltaSeconds * VelocityCurve->GetFloatValue(1 - (CurrentDistance / TotalPathLength));
		SetActorLocation(MoveSpline->GetWorldLocationAtDistanceAlongSpline(TotalPathLength - CurrentDistance));
		FRotator Rotator = MoveSpline->GetWorldRotationAtDistanceAlongSpline(TotalPathLength - CurrentDistance);
		Rotator.Yaw += 180;
		SetActorRotation(Rotator);
	}
	else
	{
		CurrentDistance += MoveSpeed * DeltaSeconds * VelocityCurve->GetFloatValue(CurrentDistance / TotalPathLength);
		SetActorLocation(MoveSpline->GetWorldLocationAtDistanceAlongSpline(CurrentDistance));
		SetActorRotation(MoveSpline->GetWorldRotationAtDistanceAlongSpline(CurrentDistance));
	}
}

void ABasicEnemy::OnBodyOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ABasicBullet* OtherBullet = Cast<ABasicBullet>(OtherActor);
	if (OtherBullet)
	{
		if (OtherBullet->bFromPlayer)
		{
			//consume the damage
			CurrentHealth -= OtherBullet->SingleDamage;
			OtherBullet->Deactivate();
			if (CurrentHealth <= 0)
			{
				//probably play some dead effect
				Deactivate();
			}
			else
			{
				//probably play some hit effect
			}
		}
	}
}

