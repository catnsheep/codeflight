// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "Public/Character/PlayerCharacter.h"
#include "Character/BasicEnemy.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	//static mesh setup
	CharacterMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CharacterMesh"));
	CharacterMesh->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
	CharacterMesh->AttachTo(RootComponent);
	//collider setup
	HitSphere = CreateDefaultSubobject<UCapsuleComponent>(TEXT("HitCapsule"));
	HitSphere->OnComponentBeginOverlap.AddDynamic(this, &APlayerCharacter::OnHitDetected);
	HitSphere->AttachTo(RootComponent);
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	MountWeapon(BasicWeapon);
	CurrentHealth = MaxHealthPoint;
}

// Called every frame
void APlayerCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void APlayerCharacter::Fire(float DeltaSeconds)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Fire(DeltaSeconds);
	}
}

void APlayerCharacter::MountWeapon(TSubclassOf<ABasicWeapon> Weapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
	}
	UWorld* const World = GetWorld();
	CurrentWeapon = World->SpawnActor<ABasicWeapon>(*Weapon, GetActorLocation(), GetActorRotation());
	CurrentWeapon->OwningPlayer = this;
	CurrentWeapon->bOwnedByPlayer = true;
}

void APlayerCharacter::OnHitDetected(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ABasicEnemy* OtherEnemy = Cast<ABasicEnemy>(OtherActor);
	if (OtherEnemy)
	{
		HitWithDamage(100);
	}
}

void APlayerCharacter::HitWithDamage(float damage)
{
	CurrentHealth -= damage;
	if (CurrentHealth <= 0)
	{
		HitSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		CharacterMesh->SetVisibility(false, true);
	}
}
