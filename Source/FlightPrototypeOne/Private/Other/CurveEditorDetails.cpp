// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "CurveEditorDetails.h"


TSharedRef<IDetailCustomization> FCurveEditorDetails::MakeInstance()
{
	return MakeShareable(new FCurveEditorDetails);
}

void FCurveEditorDetails::CustomizeDetails(IDetailLayoutBuilder& DetailBuilder)
{
	AssetName = TEXT("NewCurveDataAsset");

	// Create a category
	IDetailCategoryBuilder& MyCategory = DetailBuilder.EditCategory(FName("Curve Data"));
	MyCategory.AddCustomRow(FText::FromString("Data Export"))
	[
		SNew(SVerticalBox)
		+SVerticalBox::Slot()
		.Padding(5, 2)
		[
			SNew(SBox)
			.Content()
			[
				SNew(STextBlock)
				.Text(FText::FromString("Asset Name"))
			]
		]
		+ SVerticalBox::Slot()
		.Padding(5, 2)
		[
			SNew(SBox)
			.WidthOverride(250)
			.Content()
			[
				SNew(SEditableTextBox)
				.Text(FText::FromString(AssetName))
				.MinDesiredWidth(200)
				.OnTextChanged(this, &FCurveEditorDetails::OnAssetNameChanged)
				.OnTextCommitted(this, &FCurveEditorDetails::OnAssetNameCommitted)
			]
		]
		+SVerticalBox::Slot()
		.Padding(5, 2)
		[
			SNew(SBox)
			.Content()
			[
				SNew(SButton)
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Center)
				.OnClicked(this, &FCurveEditorDetails::OnGenerateDataAsset)
				.Text(FText::FromString("Generate Curve File"))
			]
		]
	];
	
	TArray<TWeakObjectPtr<UObject>> SelectedObjects;
	DetailBuilder.GetObjectsBeingCustomized(SelectedObjects);
	for (int ObjectIndex = 0; ObjectIndex < SelectedObjects.Num(); ++ObjectIndex)
	{
		TWeakObjectPtr<UObject>& CurrentObject = SelectedObjects[ObjectIndex];
		if (CurrentObject.IsValid())
		{
			CurveEditor = Cast<ACurveEditor>(CurrentObject.Get());
			if (CurveEditor != nullptr)
			{
				break;
			}
		}
	}

}

FReply FCurveEditorDetails::OnGenerateDataAsset()
{
	CurveEditor->SaveCurveDataAsset(AssetName);
	return FReply::Handled();
}

void FCurveEditorDetails::OnAssetNameChanged(const FText& Text)
{
	AssetName = Text.ToString();
}

void FCurveEditorDetails::OnAssetNameCommitted(const FText& Text, ETextCommit::Type CommitType)
{
	AssetName = Text.ToString();
}
