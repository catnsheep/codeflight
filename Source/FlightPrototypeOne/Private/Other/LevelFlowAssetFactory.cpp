// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "LevelFlowAssetFactory.h"
#include "LevelFlowAsset.h"

ULevelFlowAssetFactory::ULevelFlowAssetFactory()
{
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = ULevelFlowAsset::StaticClass();
}

UObject* ULevelFlowAssetFactory::FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	ULevelFlowAsset* NewObjectAsset = NewObject<ULevelFlowAsset>(InParent, InClass, InName, Flags | RF_Transactional);
	return NewObjectAsset;
}


