// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "CurveEditor.h"
#include "DrawDebugHelpers.h"
#include "CurveAsset.h"
#include "AssetRegistryModule.h"
#include "CurveEditorDetails.h"

// Sets default values
ACurveEditor::ACurveEditor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	CustomSpline = CreateDefaultSubobject<USplineComponent>(TEXT("CustomSpline"));
	CustomSpline->AttachTo(RootComponent);

	BoundarySize = FVector(100, 100, 100);

	FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyModule.RegisterCustomClassLayout("CurveEditor", FOnGetDetailCustomizationInstance::CreateStatic(&FCurveEditorDetails::MakeInstance));
}

// Called when the game starts or when spawned
void ACurveEditor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACurveEditor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	
}

#if WITH_EDITOR
void ACurveEditor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	UpdateBoundary();
}

void ACurveEditor::PostEditMove(bool bFinished)
{
	Super::PostEditMove(bFinished);

	if (bFinished)
	{
		ConstraintSpline();
	}
}

#endif

void ACurveEditor::UpdateBoundary()
{
	const UWorld* World = GetWorld();
	FVector ActorOrigin = GetActorLocation();
	FVector HalfBoundary = BoundarySize / 2;
	FlushPersistentDebugLines(World);
	DrawDebugBox(World, ActorOrigin + HalfBoundary, HalfBoundary, FColor::Red, true);
	if (ShowHalfYAxis)
	{
		HalfBoundary.Y /= 2;
		DrawDebugBox(World, ActorOrigin + HalfBoundary, HalfBoundary, FColor::Red, true);
	}
}

void ACurveEditor::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	UpdateBoundary();
}

void ACurveEditor::BeginDestroy()
{
#if WITH_EDITOR
	FlushPersistentDebugLines(GetWorld());
#endif

	Super::BeginDestroy();
}

void ACurveEditor::SaveCurveDataAsset(FString& FileName)
{
	FString PackageName = TEXT("/Game/Other/TestCurve");
	UPackage* Package = CreatePackage(NULL, *PackageName);

	UCurveAsset* NewCurveAsset = NewObject<UCurveAsset>(Package, UCurveAsset::StaticClass(), FName(*FileName), RF_Public | RF_Standalone|RF_Transactional);

	if (NewCurveAsset != nullptr)
	{
		NewCurveAsset->RangeContext = BoundarySize;
		FVector BoundarySizeHalf = BoundarySize / 2;

		for (auto& CurvePoint : CustomSpline->SplineInfo.Points)
		{
			FCurvePoint CurvePointData;
			CurvePointData.ArriveTangent = (CurvePoint.ArriveTangent - BoundarySizeHalf) / BoundarySizeHalf;
			CurvePointData.LeaveTangent = (CurvePoint.LeaveTangent - BoundarySizeHalf) / BoundarySizeHalf;
			CurvePointData.Location = (CurvePoint.OutVal - BoundarySizeHalf) / BoundarySizeHalf;
			CurvePointData.InterpMode = CurvePoint.InterpMode;

			NewCurveAsset->CurvePoints.Add(CurvePointData);
		}

	}

	FAssetRegistryModule::AssetCreated(NewCurveAsset);
	Package->MarkPackageDirty();

}

void ACurveEditor::ConstraintSpline()
{

	for (auto& CurvePoint : CustomSpline->SplineInfo.Points)
	{
		FVector Current = CurvePoint.OutVal;

		Current.X = FMath::Clamp(Current.X, 0.0f, BoundarySize.X);
		Current.Y = FMath::Clamp(Current.Y, 0.0f, BoundarySize.Y);
		Current.Z = FMath::Clamp(Current.Z, 0.0f, BoundarySize.Z);

		if (Current != CurvePoint.OutVal)
		{
			CurvePoint.OutVal = Current;
			CustomSpline->UpdateSpline();
		}
	}
}
