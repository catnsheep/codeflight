// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "CurveAssetFactory.h"
#include "CurveAsset.h"

UCurveAssetFactory::UCurveAssetFactory()
{
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = UCurveAsset::StaticClass();
}

UObject* UCurveAssetFactory::FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	UCurveAsset* NewObjectAsset = NewObject<UCurveAsset>(InParent, InClass, InName, Flags | RF_Transactional);
	return NewObjectAsset;
}
