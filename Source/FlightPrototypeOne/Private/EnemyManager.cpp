// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "Public/EnemyManager.h"
#include "Other/CurveAsset.h"
#include "DrawDebugHelpers.h"
#include "Public/Item/PoolManager.h"

// Sets default values
AEnemyManager::AEnemyManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemyManager::BeginPlay()
{
	Super::BeginPlay();
	
	InProgressGameTime = 0;
	nextWave = 0;
}

// Called every frame
void AEnemyManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	//cache the game state
	AFlightGameState* FGS = Cast<AFlightGameState>(GetWorld()->GetGameState());
	if (FGS && FGS->CurrentState==EGameStatus::GameInProgress)
	{
		InProgressGameTime += DeltaTime;
		while (nextWave<LevelDefinition->Waves.Num() && LevelDefinition->Waves[nextWave].TimeInSeconds < InProgressGameTime)
		{
			GenerateWaveEnemies(LevelDefinition->Waves[nextWave]);
			nextWave++;
		}
	}

}

void AEnemyManager::Init(APlayerControlCenter* PCC)
{
	//initialize enemy move area
	PlayerControlCenter = PCC;

	float VerticalAreaMax;
	float VerticalAreaMin;
	float HorizontalAreaMax;
	float HorizontalAreaMin;

	PlayerControlCenter->GetMoveAreaBoundary(HorizontalAreaMin, HorizontalAreaMax, VerticalAreaMax, VerticalAreaMin);

	VerticalHalf = VerticalAreaMax + VerticalAreaMin;
	HorizontalMin = HorizontalAreaMin;
	HorizontalDiff = 2 * (HorizontalAreaMax - HorizontalAreaMin);

	CenterReferencePoint = PlayerControlCenter->GetActorLocation();
	CenterReferencePoint.Y = CenterReferencePoint.Y + VerticalAreaMax;

	//initialize curve data
	for (auto& SingleWave : LevelDefinition->Waves)
	{
		FString Name = SingleWave.CurveAsset->GetName();
		if (!SplineTable.Contains(Name))
		{
			USplineComponent* SplineCompoment = NewObject<USplineComponent>(this);
			SplineCompoment->RegisterComponent();
			SplineCompoment->ClearSplinePoints();
			int index = 0;
			for (auto& PointData : SingleWave.CurveAsset->CurvePoints)
			{
				SplineCompoment->AddSplineWorldPoint(SplinePointToAreaPoint(PointData.Location));
				if (PointData.InterpMode != CIM_CurveAuto)
				{
					SplineCompoment->SplineInfo.Points[index].InterpMode = PointData.InterpMode;
					SplineCompoment->SplineInfo.Points[index].ArriveTangent = SplineCompoment->ComponentToWorld.InverseTransformPosition(SplinePointToAreaPoint(PointData.ArriveTangent));
					SplineCompoment->SplineInfo.Points[index].LeaveTangent = SplineCompoment->ComponentToWorld.InverseTransformPosition(SplinePointToAreaPoint(PointData.LeaveTangent));
				}
				index++;
			}

			SplineTable.Add(Name, SplineCompoment);
		}
	}

#if WITH_EDITOR
	DrawEnemyMoveArea();
#endif

}

#if WITH_EDITOR
void AEnemyManager::DrawEnemyMoveArea()
{
	const UWorld*  World = GetWorld();

	DrawDebugLine(World, FVector(-HorizontalMin, CenterReferencePoint.Y - VerticalHalf, 0), FVector(HorizontalMin, CenterReferencePoint.Y - VerticalHalf, 0), FColor::Green, true);
	DrawDebugLine(World, FVector(-(HorizontalMin + HorizontalDiff), CenterReferencePoint.Y + VerticalHalf, 0), FVector(HorizontalMin + HorizontalDiff, CenterReferencePoint.Y + VerticalHalf, 0), FColor::Green, true);
	
	DrawDebugLine(World, FVector(-HorizontalMin, CenterReferencePoint.Y - VerticalHalf, 0), FVector(-(HorizontalMin + HorizontalDiff), CenterReferencePoint.Y + VerticalHalf, 0), FColor::Green, true);
	DrawDebugLine(World, FVector(HorizontalMin, CenterReferencePoint.Y - VerticalHalf, 0), FVector(HorizontalMin + HorizontalDiff, CenterReferencePoint.Y + VerticalHalf, 0), FColor::Green, true);
}
#endif

FVector AEnemyManager::SplinePointToAreaPoint(FVector& Pos)
{
	return FVector(Pos.X * (((Pos.Y + 1) / 2)*HorizontalDiff + HorizontalMin) + CenterReferencePoint.X, Pos.Y * VerticalHalf + CenterReferencePoint.Y, Pos.Z * ZSpaceExtension + CenterReferencePoint.Z);
}


void AEnemyManager::GenerateSingleEnemy(FWaveEnemy* EnemyWave)
{
	APoolManager* PoolInstance = APoolManager::GetInstance();
	if (PoolInstance)
	{
		USplineComponent* SplineToUse = *SplineTable.Find(EnemyWave->CurveAsset->GetName());
		ABasicEnemy* NewEnemy = Cast<ABasicEnemy>(PoolInstance->TryGenerateFromPoolNoPos(EnemyWave->EnemyType));
		NewEnemy->Init(SplineToUse, EnemyWave->ReverseTravel, EnemyWave->VelocityBalancingCurve);
	}
}

void AEnemyManager::GenerateWaveEnemies(FWaveEnemy& EnemyWave)
{
	//spawn the first one at once
	GenerateSingleEnemy(&EnemyWave);

	//spawn the others with delay
	FTimerDelegate EnemyRespownDelegate = FTimerDelegate::CreateUObject(this, &AEnemyManager::GenerateSingleEnemy, &EnemyWave);
	for (int i = 1; i < EnemyWave.NumberToGenerate; i++)
	{
		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, EnemyRespownDelegate, i * EnemyWave.GenerateDelay, false);
	}
}
