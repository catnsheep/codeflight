// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "Public/FlightGameMode.h"
#include "Public/EnvironmentManager.h"


// Sets default values
AEnvironmentManager::AEnvironmentManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AEnvironmentManager::BeginPlay()
{
	Super::BeginPlay();
	
	UWorld* World = GetWorld();
	AFlightGameMode* const FlightGameMode = GetWorld()->GetAuthGameMode<AFlightGameMode>();
	if (World&&FlightGameMode&&GroundActor)
	{
		float EnvironmentSpeed = FlightGameMode->EnvironmentMoveSpeed;

		AEnvironmentActor* GroundActorDefault = GroundActor->GetDefaultObject<AEnvironmentActor>();
		if (GroundActorDefault)
		{
			float MinY = GroundActorDefault->ResetLimit;
			float MaxY = GroundActorDefault->ForwardLimit;
			float SStep = GroundActorDefault->SpawnStep;

			for (float k = 0; k < GroundActorDefault->Lines.Num(); k++)
			{
				//spawn left side
				for (float i = MinY; i < MaxY - SStep; i = i + SStep)
				{
					i += FMath::FRandRange(0.f, SStep);

					AEnvironmentActor* NewGroundObject = World->SpawnActor<AEnvironmentActor>(GroundActor);
					NewGroundObject->ResetActor(k, i, EnvironmentSpeed);
					EnvironmentActors.Add(NewGroundObject);
				}
			}

		}

	}

}

// Called every frame
void AEnvironmentManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

