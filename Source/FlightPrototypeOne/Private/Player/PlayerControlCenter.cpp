// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "PlayerControlCenter.h"
#include "Public/Character/PlayerCharacter.h"
#include "DrawDebugHelpers.h"

// Sets default values
APlayerControlCenter::APlayerControlCenter()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	//springarm component setup
	MainCameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("MainCameraSpringArm"));
	MainCameraSpringArm->SetRelativeLocationAndRotation(FVector(0.f, 0.f, 0.f), FRotator(-40.f, 0.f, 0.f));
	MainCameraSpringArm->TargetArmLength = 100.f;
	MainCameraSpringArm->bEnableCameraLag = true;
	MainCameraSpringArm->CameraLagSpeed = 1.f;
	MainCameraSpringArm->bEnableCameraRotationLag = true;
	MainCameraSpringArm->CameraRotationLagSpeed = 5.f;
	MainCameraSpringArm->AttachTo(RootComponent);

	//camera component setup
	MainCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCamera"));
	MainCamera->AttachTo(MainCameraSpringArm, USpringArmComponent::SocketName);

	//spaceship object setup
	PlayerCharacter = CreateDefaultSubobject<UChildActorComponent>("PlayerCharacter");
	PlayerCharacter->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
	PlayerCharacter->AttachTo(RootComponent);

	WidthConstraint = 175.f;
	CharacterMoveSpeed = 30.f;
	RollLimit = 45.f;
	RollSpeed = 5;

	CurRollTarget = 0;
	bIsFiring = false;

	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void APlayerControlCenter::BeginPlay()
{
	Super::BeginPlay();

	//setup movement constraint
	HeightConstraint = WidthConstraint / MainCamera->AspectRatio;
	FVector IniLocation = GetActorLocation();
	CharacterMoveAreaMax = FVector(WidthConstraint / 2, HeightConstraint / 2, IniLocation.Z);
	CharacterMoveAreaMin = FVector(-WidthConstraint / 2, -HeightConstraint / 2, IniLocation.Z);

	//adjust camera based on Screen Size
	HFOVRadiansHalf = FMath::Atan((WidthConstraint / 2.f) / MainCameraSpringArm->TargetArmLength);
	MainCamera->FieldOfView = FMath::RadiansToDegrees(HFOVRadiansHalf) * 2;
	VerticalFOVHalf = FMath::RadiansToDegrees(FMath::Atan((HeightConstraint / 2.f) / MainCameraSpringArm->TargetArmLength));

	CameraRelativeHeight = MainCamera->GetComponentLocation().Z - MainCameraSpringArm->GetComponentLocation().Z;

	CameraVerticalMin = VerticalFOVHalf - 90;
	//CameraVerticalMax = -FMath::RadiansToDegrees(FMath::Atan(CameraRelativeHeight / HeightConstraint));
	//CameraVerticalMax = -50;
	CameraVerticalMax = -FMath::RadiansToDegrees(FMath::Atan(CameraRelativeHeight / (2 * HeightConstraint))) - VerticalFOVHalf;

	if (CameraVerticalMax < CameraVerticalMin)
	{
		CameraVerticalMax = CameraVerticalMin + 1;
	}

	AdjustCamera();

	//cache overall limitations
	VerticalMoveMax = HeightConstraint / 2;
	VerticalMoveMin = HeightConstraint / 2;
	HorizontalMoveMax = FMath::Tan(HFOVRadiansHalf) * FMath::Sqrt(HeightConstraint *  HeightConstraint + CameraRelativeHeight * CameraRelativeHeight);
	HorizontalMoveMin = FMath::Tan(HFOVRadiansHalf) * CameraRelativeHeight;

#if WITH_EDITOR
	DrawMoveArea();
#endif

}

// Called every frame
void APlayerControlCenter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	HandleCharacterMovement(DeltaTime);
	HoldCharacterInCamera(DeltaTime);

	/*
	GEngine->AddOnScreenDebugMessage(2, 5.f, FColor::Red, FString::Printf(TEXT("Character World Location: %s"), *PlayerCharacter->GetComponentLocation().ToString()));
	GEngine->AddOnScreenDebugMessage(3, 5.f, FColor::Red, FString::Printf(TEXT("Camera World Location: %s"), *MainCamera->GetComponentLocation().ToString()));
	GEngine->AddOnScreenDebugMessage(6, 5.f, FColor::Red, FString::Printf(TEXT("Horizontal Move Limit: %f, %f"), CharacterMoveAreaMin.X, CharacterMoveAreaMax.X));
	*/

	if (bIsFiring)
	{
		APlayerCharacter* PC = Cast<APlayerCharacter>(PlayerCharacter->ChildActor);
		if (PC)
		{
			PC->Fire(DeltaTime);
		}
	}

}

// Called to bind functionality to input
void APlayerControlCenter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	//movement input
	InputComponent->BindAxis("HorizontalMove", this, &APlayerControlCenter::HorizontalMove);
	InputComponent->BindAxis("VerticalMove", this, &APlayerControlCenter::VerticalMove);
	//fire
	InputComponent->BindAction("Fire", IE_Pressed, this, &APlayerControlCenter::StartFiring);
	InputComponent->BindAction("Fire", IE_Released, this, &APlayerControlCenter::StopFiring);

}

void APlayerControlCenter::AdjustCamera()
{
	
	FVector LocalPlayerLocation = PlayerCharacter->GetRelativeTransform().GetLocation();
	FVector PositionRatio = (LocalPlayerLocation - CharacterMoveAreaMin) / (CharacterMoveAreaMax - CharacterMoveAreaMin);

	FVector DeltaDist = PlayerCharacter->ChildActor->GetActorLocation() - FVector(0.f, CharacterMoveAreaMin.Y, CameraRelativeHeight);
	float RawRadians = -FMath::Atan(DeltaDist.Y / DeltaDist.Z);
	
	if (RawRadians < 0)
	{
		RawRadians = 0;
	}
	
	float RawDegree = FMath::RadiansToDegrees(RawRadians);
	float CameraDownAngle = FMath::Lerp(CameraVerticalMin, CameraVerticalMax, PositionRatio.Y);

	MainCameraSpringArm->SetRelativeRotation(FRotator(CameraDownAngle, 90, 0));

	MainCameraSpringArm->TargetArmLength = CameraRelativeHeight / FMath::Sin(FMath::DegreesToRadians(-CameraDownAngle));
	
	float HoriLimit = (CameraRelativeHeight / FMath::Cos(RawRadians)) * FMath::Tan(HFOVRadiansHalf);
	CharacterMoveAreaMin.X = -HoriLimit;
	CharacterMoveAreaMax.X = HoriLimit;

}

void APlayerControlCenter::VerticalMove(float AxisValue)
{
	MovementInput.Y = FMath::Clamp<float>(AxisValue, -1.f, 1.f);
}

void APlayerControlCenter::HorizontalMove(float AxisValue)
{
	MovementInput.X = -FMath::Clamp<float>(AxisValue, -1.f, 1.f);
}

void APlayerControlCenter::HandleCharacterMovement(float DeltaTime)
{
	FTransform CharacterRelativeTransform = PlayerCharacter->GetRelativeTransform();

	if (!MovementInput.IsZero())
	{
		//adjust location
		FVector NormalMoveInput = MovementInput.GetSafeNormal();
		FVector SpeedVector = NormalMoveInput * CharacterMoveSpeed * DeltaTime;
		FVector CharacterLocation = CharacterRelativeTransform.GetLocation();

		//Handle seperately because x movement limit is also related with camera rotation, which is constantly changing
		if (CharacterLocation.X > CharacterMoveAreaMin.X && CharacterLocation.X < CharacterMoveAreaMax.X)
		{
			CharacterLocation.X += SpeedVector.X;
			CharacterLocation.X = FMath::Clamp<float>(CharacterLocation.X, CharacterMoveAreaMin.X, CharacterMoveAreaMax.X);
		}
		else
		{
			if (CharacterLocation.X < CharacterMoveAreaMin.X)
			{
				CharacterLocation.X += (CharacterMoveSpeed / 2) * DeltaTime;
			}
			else
			{
				CharacterLocation.X -= (CharacterMoveSpeed / 2) * DeltaTime;
			}
		}

		CharacterLocation.Y += SpeedVector.Y;
		//constraint the minimum vertical move area to 90% as a work around for an camera issue
		CharacterLocation.Y = FMath::Clamp<float>(CharacterLocation.Y, CharacterMoveAreaMin.Y * 0.9f, CharacterMoveAreaMax.Y);

		PlayerCharacter->SetRelativeLocation(CharacterLocation);
		//adjust horizontal rotation
		CurRollTarget = -RollLimit * NormalMoveInput.X;
		//adjust camera position
		AdjustCamera();
	}
	else
	{
		CurRollTarget = 0;
	}

	float CurRoll = CharacterRelativeTransform.Rotator().Pitch;
	CurRoll = FMath::FInterpTo(CurRoll, CurRollTarget, DeltaTime, RollSpeed);
	PlayerCharacter->SetRelativeRotation(FRotator(CurRoll, 0.f, 0.f));
}

void APlayerControlCenter::HoldCharacterInCamera(float DeltaTime)
{
	FVector CharacterLocation = PlayerCharacter->RelativeLocation;
	if (CharacterLocation.X < CharacterMoveAreaMin.X)
	{
		CharacterLocation.X += CharacterMoveSpeed * DeltaTime;
	}
	else if (CharacterLocation.X > CharacterMoveAreaMax.X)
	{
		CharacterLocation.X -= CharacterMoveSpeed * DeltaTime;
	}
	PlayerCharacter->SetRelativeLocation(CharacterLocation);
}

void APlayerControlCenter::StartFiring()
{
	bIsFiring = true;
}

void APlayerControlCenter::StopFiring()
{
	bIsFiring = false;
}

#if WITH_EDITOR
void APlayerControlCenter::DrawMoveArea()
{
	const UWorld* World = GetWorld();

	DrawDebugLine(World, FVector(-HorizontalMoveMax, VerticalMoveMax, 0), FVector(HorizontalMoveMax, VerticalMoveMax, 0), FColor::Green, true);
	DrawDebugLine(World, FVector(-HorizontalMoveMin, -VerticalMoveMin, 0), FVector(HorizontalMoveMin, -VerticalMoveMin, 0), FColor::Green, true);
	DrawDebugLine(World, FVector(-HorizontalMoveMin, -VerticalMoveMin, 0), FVector(-HorizontalMoveMax, VerticalMoveMax, 0), FColor::Green, true);
	DrawDebugLine(World, FVector(HorizontalMoveMin, -VerticalMoveMin, 0), FVector(HorizontalMoveMax, VerticalMoveMax, 0), FColor::Green, true);
}
#endif


void APlayerControlCenter::GetMoveAreaBoundary(float& OutHorizontalMoveMin, float& OutHorizontalMoveMax, float& OutVerticalMoveMax, float& OutVerticalMoveMin)
{
	OutHorizontalMoveMin = HorizontalMoveMin;
	OutHorizontalMoveMax = HorizontalMoveMax;
	OutVerticalMoveMax = VerticalMoveMax;
	OutVerticalMoveMin = VerticalMoveMin;
}