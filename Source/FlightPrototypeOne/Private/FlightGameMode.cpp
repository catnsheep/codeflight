// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "FlightGameMode.h"
#include "EnemyManager.h"
#include "Player/PlayerControlCenter.h"
#include "FlightGameState.h"

AFlightGameMode::AFlightGameMode(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	EnvironmentMoveSpeed = 10.f;
}

void AFlightGameMode::StartPlay()
{
	Super::StartPlay();

	UWorld* World = GetWorld();

	APlayerControlCenter* NewPlayerControlCenter = World->SpawnActor<APlayerControlCenter>(PlayerControlCenter, FVector::ZeroVector, FRotator::ZeroRotator);
	AEnemyManager* NewEnemyManager = World->SpawnActor<AEnemyManager>(EnemyManager, FVector::ZeroVector, FRotator::ZeroRotator);
	NewEnemyManager->Init(NewPlayerControlCenter);

	AFlightGameState* FGS = Cast<AFlightGameState>(GetWorld()->GetGameState());
	if (FGS)
	{
		FGS->CurrentState = EGameStatus::GameInProgress;
	}
}

