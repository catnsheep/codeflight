// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "Public/Item/BasicBullet.h"

ABasicBullet::ABasicBullet()
{
	MoveSpeed = 100.f;
}

void ABasicBullet::BeginPlay()
{
	Super::BeginPlay();
}

void ABasicBullet::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bActivated)
	{
		FVector ActorLocation = GetActorLocation();
		ActorLocation += GetActorRightVector() * MoveSpeed * DeltaSeconds;
		SetActorLocation(ActorLocation);
	}
}