// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "Public/Item/BasicWeapon.h"
#include "Public/Item/PoolManager.h"
#include "Public/Item/BasicBullet.h"

// Sets default values
ABasicWeapon::ABasicWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	DefaultFireDelay = 2.f;
}

// Called when the game starts or when spawned
void ABasicWeapon::BeginPlay()
{
	Super::BeginPlay();
	FireDelay = 0;
}

// Called every frame
void ABasicWeapon::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ABasicWeapon::Fire(float DeltaSeconds)
{
	if (FireDelay > 0)
	{
		FireDelay -= DeltaSeconds;
	}
	else
	{
		APoolManager* PoolInstance = APoolManager::GetInstance();
		if (PoolInstance)
		{
			for (auto& WeaponSocket : SocketDefinition)
			{
				ABasicBullet* NewBullet = Cast<ABasicBullet>(PoolInstance->TryGenerateFromPool(WeaponSocket.BulletType, WeaponSocket.SpawnLocation + OwningPlayer->GetActorLocation(), WeaponSocket.SpawnRotation));
				if (NewBullet)
				{
					NewBullet->bFromPlayer = bOwnedByPlayer;
				}
			}
		}
		FireDelay = DefaultFireDelay;
	}
}
