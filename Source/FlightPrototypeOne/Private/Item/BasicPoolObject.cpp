// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "Public/Item/BasicPoolObject.h"

// Sets default values
ABasicPoolObject::ABasicPoolObject()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	lifeTime = -1;
	bActivated = false;

	bDynamicGenerated = false;
}

// Called when the game starts or when spawned
void ABasicPoolObject::BeginPlay()
{
	Super::BeginPlay();

}


// Called every frame
void ABasicPoolObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bActivated)
	{

		if (lifeTime > 0)
		{
			lifeTime -= DeltaTime;
			if (lifeTime <= 0)
			{
				Deactivate();
			}
		}

	}
}

bool ABasicPoolObject::Activate(FVector IniLocation, FRotator IniRotation, float IniLifeTime, bool bDynamicGen)
{
	if (bActivated)
	{
		return false;
	}
	else
	{
		bDynamicGenerated = bDynamicGen;
		SetActorLocation(IniLocation);
		SetActorRotation(IniRotation);
		if (IniLifeTime > 0)
		{
			lifeTime = IniLifeTime;
		}
		else
		{
			lifeTime = MaxLifeTime;
		}
		
		//CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		SetActorEnableCollision(true);
		SetActorTickEnabled(true);
		SetActorHiddenInGame(false);
		bActivated = true;
		return true;
	}
}

void ABasicPoolObject::Deactivate()
{
	if (bDynamicGenerated)
	{
		Destroy();
	}
	else
	{
		//CollisionComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		lifeTime = -1;
		bActivated = false;
		
		SetActorEnableCollision(false);
		SetActorTickEnabled(false);
		SetActorHiddenInGame(true);

		SetActorLocation(InValidLocation);
		AttachRootComponentToActor(InValidParent);
	}
}

bool ABasicPoolObject::IsActivated()
{
	return bActivated;
}
