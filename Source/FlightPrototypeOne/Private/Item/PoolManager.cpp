// Copyright 2015, Maoyang Li. All Rights Reserved

#include "FlightPrototypeOne.h"
#include "Public/Item/PoolManager.h"

APoolManager* APoolManager::instance = nullptr;

// Sets default values
APoolManager::APoolManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	instance = this;
}

// Called when the game starts or when spawned
void APoolManager::BeginPlay()
{
	Super::BeginPlay();
	
	UWorld* const World = GetWorld();
	//Place all invalid pool objects at the place of the manager 
	FVector GatherLocation = GetActorLocation();

	for (auto& PoolObjectEntry : PoolObjectEntrys)
	{
		TSharedPtr<TArray<ABasicPoolObject*>> PoolObjects(new TArray<ABasicPoolObject*>());
		for (int32 i = PoolObjectEntry.InitialAmount; i > 0; i--)
		{
			ABasicPoolObject* NewPoolObject = World->SpawnActor<ABasicPoolObject>(*PoolObjectEntry.PoolObject, GatherLocation, FRotator::ZeroRotator);
			if (NewPoolObject)
			{
				NewPoolObject->InValidLocation = GatherLocation;
				NewPoolObject->InValidParent = this;
				NewPoolObject->Deactivate();

				PoolObjects->Add(NewPoolObject);
			}
		}
		PoolTable.Add((*PoolObjectEntry.PoolObject)->GetName(), PoolObjects);
	}

}

/*
// Called every frame
void APoolManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}
*/

ABasicPoolObject* APoolManager::TryGenerateFromPoolNoPos(TSubclassOf<ABasicPoolObject> Target, float IniLifeTime /* = -1 */)
{
	return TryGenerateFromPool(Target, GetActorLocation(), FRotator::ZeroRotator);
}

ABasicPoolObject* APoolManager::TryGenerateFromPool(TSubclassOf<ABasicPoolObject> Target, FVector IniLocation, FRotator IniRotation, float IniLifeTime)
{
	TSharedPtr<TArray<ABasicPoolObject*>>* PoolEntry = PoolTable.Find((*Target)->GetName());
	if (PoolEntry)
	{
		for (auto& PoolObject : **PoolEntry)
		{
			if (!PoolObject->IsActivated())
			{
				PoolObject->Activate(IniLocation, IniRotation, IniLifeTime, false);
				return PoolObject;
			}
		}
	}

	//fail to locate an entry or no de-activate item available, just spawn a new one
	UWorld* const World = GetWorld();
	ABasicPoolObject* NewSpawnObject = World->SpawnActor<ABasicPoolObject>(*Target, IniLocation, FRotator::ZeroRotator);
	if (NewSpawnObject)
	{
		NewSpawnObject->Activate(IniLocation, IniRotation, IniLifeTime, true);
		return NewSpawnObject;
	}

	//fail to spawn anyway for some reason
	return nullptr;

}

APoolManager* APoolManager::GetInstance()
{
	return instance;
}
