// Copyright 2015, Maoyang Li. All Rights Reserved

using UnrealBuildTool;
using System.Collections.Generic;

public class FlightPrototypeOneEditorTarget : TargetRules
{
	public FlightPrototypeOneEditorTarget(TargetInfo Target)
	{
		Type = TargetType.Editor;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.AddRange( new string[] { "FlightPrototypeOne" } );
	}
}
