# Project Flight
------------------------------------

**Project Flight** is a personal project that I am working on alone recently. 

It is a flight shooting game based on Unreal Engine 4.9 featuring the use of 45-degree top-down perspective camera in traditional space shooter style game. Now it is still in an early development stage that I have not put in many art assets or effects yet. I am still actively working on the underlying framework, subsystems and tools for the game.

Here is the introduction post about this project on my personal website:  [www.imaoyang.com/project-flight/](http://www.imaoyang.com/project-flight/)  
Here is an early technical demo showing some of the completed features:  [youtu.be/IPOlZWjOyLM](https://youtu.be/IPOlZWjOyLM)  

For now the completed features include:  
(And related files in "FlightPrototypeOne" folder)

* Customize curve editor tool for Unreal Engine Editor 

>    * Public/Other/CurveAsset.h
>    * Public/Other/CurveAssetFactory.h
>    * Public/Other/CurveEditor.h
>    * Public/Other/CurveEditorDetails.h

* Player move area based on screen ratio and camera perspective

>    * Public/Player/PlayerControlCenter.h

* Object-oriented framework that supports multiple weapons, bullet types, enemies and characters 

>    * Public/Character/PlayerCharacter.h
>    * Public/Item/BasicBullet.h
>    * Public/Item/BasicWeapon.h
>    * Public/Character/BasicEnemy.h

* Object pool

>    * Public/Item/BasicPoolObject.h
>    * Public/Item/PoolManager.h

* Data-driven level design

>    * Public/Other/LevelFlowAsset.h
>    * Public/Other/LevelFlowAssetFactory.h
>    * Public/Other/EnemyManager.h

* Infinity "landscape"

>    * Public/EnvironmentActor.h
>    * Public/EnvironmentManager.h

----------------------------------------
Maoyang Li  
Software Engineer